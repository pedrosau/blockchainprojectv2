import hashlib
import time

class Block:
    def update_hash(self):
        concat = str(self.timestamp) + str(self.data) + str(self.nonce) + str(self.directory_hash)
        m = hashlib.sha256()
        m.update(concat.encode('utf-8'))
        self.hash = m.hexdigest()

    def mine(self, nlz=3):
        self.update_hash()
        while not self.hash[:nlz] == "0" * nlz:
            self.nonce = str(int(self.nonce) + 1)
            self.update_hash()
            time.sleep(0)