from WebsiteBlock import DirectoryUpdate, WebsiteBlock, DirectoryUpdateType
from collections.abc import MutableMapping
import hashlib

from base64 import b64encode, b64decode
import rsa
import Node
from Crypto.PublicKey.RSA import import_key

class DirectoryEntry:
    def __init__(self, file_hash, owner_pk, signature):
        self.file_hash = str(file_hash)
        self.signature = signature
        self.owner_pk = owner_pk
    def __repr__(self):
        return f"<DirectoryEntry: [FilesHash: {self.file_hash}, OwnerPK: {self.owner_pk}, Signature: {self.signature}]>"

class Directory(MutableMapping):
    def __init__(self, blockchain=[]):
        for block in blockchain:
            self.update(block)

    def __setitem__(self, key, value):
        if isinstance(value, DirectoryEntry):
            self.__dict__[key] = value

    def __getitem__(self, key):
        return self.__dict__[key]

    def __delitem__(self, key):
        del self.__dict__[key]

    def __iter__(self):
        return iter(self.__dict__)

    def __len__(self):
        return len(self.__dict__)

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return self.__str__()

    def items(self):
        return self.__dict__.items()

    def update(self, block):
        if not block.data.split('\t')[0] == 'WEBSITE':
            return

        data = {entry.split(':')[0]: entry.split(':')[1] for entry in block.data.split('\t')[1:]}
        if int(data['TYPE']) == DirectoryUpdateType.PUBLISH.value:
            if not data['URL'] in self.__dict__:
                self[data['URL']] = DirectoryEntry(data['FILE_HASH'], data['OWNER_PK'].encode(), eval(data['SIGNATURE']))
        elif int(data['TYPE']) == DirectoryUpdateType.UPDATE.value:
            if data['URL'] in self.__dict__:
                self[data['URL']].file_hash =  data['FILE_HASH']
                self[data['URL']].signature =  eval(data['SIGNATURE'])

    def hash(self):
        concat = ''.join(str(entry) for entry in sorted(self.__dict__.items()))
        m = hashlib.sha256()
        m.update(concat.encode('utf-8'))
        return m.hexdigest()

if __name__ == '__main__':
    public, private =  Node.generate_keys()
    hash = 'abc123'
    signature = b64encode(rsa.sign(hash.encode(), import_key(private)))

    block1 = WebsiteBlock(index=1, data=DirectoryUpdate(url='google.com', file_hash='abc123', signature=bytes([1])))
    block2 = WebsiteBlock(index=2, data=DirectoryUpdate(url='yahoo.com', file_hash='123abc', signature=bytes([2])))
    block3 = WebsiteBlock(index=3, data=DirectoryUpdate(url='facebook.com', file_hash='xyz321', signature=bytes([3])))
    block4 = WebsiteBlock(index=4, data=DirectoryUpdate(url='google.com', file_hash='321xyz', signature=signature, type=DirectoryUpdateType.UPDATE))
    blockchain = [block1, block2, block3, block4]
    print(blockchain)
    d = Directory(blockchain=blockchain)