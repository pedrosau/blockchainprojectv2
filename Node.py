#!/usr/bin/env python3

import threading, time, hashlib, os, shutil, rsa, random
from Directory import DirectoryEntry, Directory
from queue import Queue
from TransactionBlock import TransactionBlock, Transaction
from WebsiteBlock import *
from uuid import uuid4
from base64 import b64encode, b64decode
from Crypto.PublicKey.RSA import import_key

lock = threading.RLock()
out_lock = threading.RLock()

def generate_keys():
	public, private = rsa.newkeys(1024)
	return public.export_key(), private.export_key()

class Peer:
	def __init__(self, id, public_key):
		self.id = id
		self.public_key = public_key

	def __eq__(self, peer):
		return self.id == peer.id and self.public_key == peer.public_key

	def __hash__(self):
		return hash(self.id) + hash(self.public_key)

class Node(threading.Thread):
	def __init__(self, network, id, public_key, private_key, instructions_queue, initial_neighbor=None, host=False, allocated_space=0):
		super().__init__()

		self.network = network
		self.id = id
		self.public_key = public_key
		self.private_key = private_key
		self.instructions_queue = instructions_queue
		self.folder_name = os.getcwd() + "/WebPages/node_" + str(self.id)
		self.running = True
		self.neighbors = set()
		self.blockchain = []
		self.blockchain_lock = threading.RLock()
		self.mining_lock = threading.RLock()
		self.directory = None
		self.host = host
		self.allocated_space = allocated_space
		self.hosted_sites = set()
		self.owned_sites = set()
		self.received_requests = set()
		self.pending_requests = dict()
		self.mining_threads = dict()
		self.tokens = 50

		out_lock.acquire()
		print(f'> [{self.id}] Booting up.')
		out_lock.release()
		self.initialize_neighbor(initial_neighbor)

	# Contact seed neighbors. If there are none then receive original coins.
	def initialize_neighbor(self, neighbor):
		if neighbor:
			neighbor_id, neighbor_pk = neighbor
			self.contact_peer(Peer(neighbor_id, neighbor_pk))
		else:
			initial_block = TransactionBlock(index="0",
				timestamp=datetime.utcnow().timestamp(),
				data=Transaction(
				recipient=self.id,
				amount="50"))
			initial_block.mine()
			self.blockchain.append(initial_block)
			self.directory = Directory()

	# Create Node's directory and start the handler
	def run(self):
		self.create_file_directory()
		self.handler()

	# Handle requests in the queue
	def handler(self):
		while self.running:
			time.sleep(1)
			self.check_mining_threads()

			# Check Instruction Queue
			if not self.instructions_queue.empty():
				self.instructions_handler(self.instructions_queue.get())

			# Check Network Queue
			queue = self.network[self.public_key]
			if queue.empty():
				continue
			else:
				lock.acquire()
				msg = queue.get()
				lock.release()
				self.message_handler(msg)

	def message_handler(self, received_msg):
		msg_type, sender, msg = received_msg

		out_lock.acquire()
		print(f'> [{sender[0]} -> {self.id}], Message: {msg_type}')
		out_lock.release()

		if msg_type == "CONTACT_REQ":
			self.new_peer_request(msg, sender)
		elif msg_type == "REQ_ACCEPTED":
			self.peer_accepted(msg, sender)
		elif msg_type == 'BLOCKCHAIN_REQ':
			self.send_blockchain(msg, sender)
		elif msg_type == "BLOCKCHAIN_UPLOAD":
			self.receive_blockchain(msg, sender)
		elif msg_type == 'FIND_HOST_PUBLISH':
			self.handle_host_request(msg, sender)
		elif msg_type == 'FIND_HOST_VISIT':
			self.handle_visit_request(msg, sender)
		elif msg_type == 'ACCEPT_HOST':
			self.handle_host_accept(msg, sender)
		elif msg_type == 'ACCEPT_VISIT':
			self.handle_visit_accept(msg, sender)
		elif msg_type == 'VISIT_PAY':
			self.handle_visit_pay(msg, sender)
		elif msg_type == 'WEBSITE_UPLOAD_HOST':
			self.handle_host_upload(msg, sender)
		elif msg_type == 'UPDATE_NOTICE':
			self.handle_update_notice(msg, sender)
		elif msg_type == 'UNMINED_BLOCK_PUBLISHED':
			self.handle_unmined_block(msg, sender)
		elif msg_type == 'MINED_BLOCK_PUBLISHED':
			self.handle_mined_block(msg, sender)
		elif msg_type == "NODE_EXITING":
			self.neighbor_exiting(msg, sender)

	# ***** Helper Methods ******

	def send_message(self, msg_type, msg, peer):
		lock.acquire()
		queue = self.network[peer.public_key]
		lock.release()
		sender = (self.id, self.public_key)
		msg_to_send = (msg_type, sender, msg)
		queue.put(msg_to_send)

	def contact_peer(self, peer):
		msg_type = "CONTACT_REQ"
		self.send_message(msg_type, [], peer)

	def initial_block_download(self):
		peer = list(self.neighbors)[0]
		msg_type = "BLOCKCHAIN_REQ"
		self.send_message(msg_type, [], peer)

	def create_file_directory(self):
		try:
			os.mkdir(self.folder_name)
		except:
			shutil.rmtree(self.folder_name)
			os.mkdir(self.folder_name)
		os.mkdir(f'{self.folder_name}/visits')

	def verify_visit(self, url, contents):
		entry = self.directory[url]
		hash = str(hashlib.sha256(contents.encode()).hexdigest())
		return hash == entry.file_hash and rsa.verify(hash.encode(), b64decode(entry.signature), import_key(entry.owner_pk))

	def verify_update(self, url, contents, reported_hash, signature):
		entry = self.directory[url]
		hash = hashlib.sha256(contents.encode()).hexdigest()
		return hash == reported_hash and rsa.verify(reported_hash.encode(), b64decode(signature), import_key(entry.owner_pk))

	def mine_block(self, new_block, request_id):
		while True:
			time.sleep(1)
			# Update block with prev_block_hash, miner $, index
			new_block.prev_block_hash = self.blockchain[-1].hash
			new_block.index = str(len(self.blockchain)).zfill(64)

			# Mine the block until you have the right nonce
			new_block.mine()
			time.sleep(1)

			# Check flags in self.mining_threads
			self.mining_lock.acquire()
			flag = self.mining_threads[request_id][1]
			if flag:
				self.mining_lock.release()
				break
			self.mining_lock.release()

			# Checks: stop thread short if block has already been mined by another node
			queue = self.network[self.public_key]
			time.sleep(1)
			for received_msg in list(queue.queue):
				msg_type, sender, msg = received_msg
				if msg_type != "MINED_BLOCK_PUBLISHED":
					continue
				unmined_req_id = msg[2]
				# Mined Block already exists, exit thread
				if unmined_req_id == request_id:
					return

			# Checks: is prev_node hash still correct?
			self.blockchain_lock.acquire()
			last_block_hash = self.blockchain[-1].hash
			if last_block_hash != new_block.prev_block_hash:
				self.blockchain_lock.release()
				continue
			self.blockchain.append(new_block)
			self.blockchain_lock.release()

			# Update directory with new block
			self.directory.update(new_block)

			# Announce mined block to neighbors
			msg_type = "MINED_BLOCK_PUBLISHED"
			mined_request_id = uuid4()
			msg = [new_block, mined_request_id, request_id]
			for peer in self.neighbors:
				self.send_message(msg_type, msg, peer)

			# Update your local balance with your mining reward
			self.tokens += new_block.miner_reward
			out_lock.acquire()
			print(f'> [{self.id}] succesfully mined. Balance: {self.tokens}')
			out_lock.release()

			# Add mined_request_ID to received_requests
			self.received_requests.add(mined_request_id)
			return

	def check_mining_threads(self):
		to_remove = []
		for id in self.mining_threads:
			if not self.mining_threads[id][0].isAlive():
				self.mining_threads[id][0].join()
				to_remove.append(id)
		for id in to_remove:
			del self.mining_threads[id]

	# ****** Handler Methods *******

	def new_peer_request(self, msg, sender):
		peer = Peer(sender[0], sender[1])
		self.neighbors.add(peer)
		msg_type = "REQ_ACCEPTED"
		self.send_message(msg_type, [], peer)

	def peer_accepted(self, msg, sender):
		peer = Peer(sender[0], sender[1])
		self.neighbors.add(peer)
		if self.directory == None:
			self.initial_block_download()

	def send_blockchain(self, msg, sender):
		msg_type = "BLOCKCHAIN_UPLOAD"
		msg = [[block.copy() for block in self.blockchain]]
		peer = Peer(sender[0], sender[1])
		self.send_message(msg_type, msg, peer)

	def receive_blockchain(self, msg, sender):
		self.blockchain = msg[0]
		self.directory = Directory(self.blockchain)

	def neighbor_exiting(self, msg, sender):
		peer = Peer(sender[0], sender[1])
		if peer in self.neighbors:
			self.neighbors.remove(peer)

		new_peer_list = msg[0]
		for node_id, node_public_key in new_peer_list:
			peer = Peer(node_id, node_public_key)
			if not node_id == self.id and not peer in self.neighbors:
				self.contact_peer(peer)

	def handle_host_request(self, msg, sender):
		req_id = msg[0]
		needed_space = msg[1]
		requester = Peer(msg[2][0], msg[2][1])
		payment_requested = 5

		if req_id in self.received_requests:
			return
		else:
			self.received_requests.add(req_id)

		if not self.host or self.allocated_space < needed_space:
			for neighbor in [n for n in self.neighbors if not n == Peer(sender[0], sender[1])]:
				msg_type = 'FIND_HOST_PUBLISH'
				self.send_message(msg_type, msg, neighbor)
		else:
			if not requester in self.neighbors:
				self.contact_peer(requester)
			self.send_message("ACCEPT_HOST", [req_id, payment_requested], requester)
			for neighbor in [n for n in self.neighbors if not n == Peer(sender[0], sender[1])]:
				msg_type = 'FIND_HOST_PUBLISH'
				self.send_message(msg_type, msg, neighbor)

	def handle_host_accept(self, msg, sender):
		req_id = msg[0]
		payment_requested = msg[1]
		peer = Peer(sender[0], sender[1])
		req = self.pending_requests[req_id]
		url = req[0]
		file_path = req[1]
		allocated_tokens = req[2]
		if payment_requested > allocated_tokens or not req_id in self.pending_requests:
			return

		with open(file_path, 'r') as f:
			contents = f.read()
		hash = str(hashlib.sha256(contents.encode()).hexdigest())
		signature = b64encode(rsa.sign(hash.encode(), import_key(self.private_key)))

		msg_type = 'WEBSITE_UPLOAD_HOST'
		msg = [url, hash, signature, contents, payment_requested]
		self.send_message(msg_type, msg, peer)

		# Create Payment block
		payment_block = TransactionBlock(
			index=len(self.blockchain),
			previous_block_hash=self.blockchain[-1].hash,
			data=Transaction(sender=self.id, recipient=peer.id, amount=payment_requested)
		)
		self.tokens -= payment_requested
		self.pending_requests[req_id][2] -= payment_requested
		out_lock.acquire()
		print(f'> [{self.id}] paid {payment_requested} tokens. Balance: {self.tokens}')
		out_lock.release()

		if not url in self.owned_sites:
			self.owned_sites.add(url)
			# Create Block with website data
			website_block = WebsiteBlock(
				index=len(self.blockchain),
				previous_block_hash=self.blockchain[-1].hash,
				directory_hash=self.directory.hash(),
				data=DirectoryUpdate(
					type=DirectoryUpdateType.PUBLISH,
					url=url,
					file_hash=hash,
					owner_pk=self.public_key.decode(),
					signature=signature
			))
			block_id = uuid4()
			recipients = list(self.neighbors) + [Peer(self.id, self.public_key)]
			random.shuffle(recipients)
			for neighbor in recipients:
				msg_type = 'UNMINED_BLOCK_PUBLISHED'
				msg = [block_id, website_block.copy()]
				self.send_message(msg_type, msg, neighbor)


	def handle_host_upload(self, msg, sender):
		url = msg[0]
		hash = msg[1]
		owner_pk = sender[1]
		signature = msg[2]
		contents = msg[3]
		payment_requested = msg[4]

		if rsa.verify(hash.encode(), b64decode(signature), import_key(owner_pk)):
			out_lock.acquire()
			print(f'> [{self.id}] downloading website from node {sender[0]}')
			out_lock.release()
			os.mkdir(f'{self.folder_name}/{url}')
			f = open(f'{self.folder_name}/{url}/index.html', 'w+')
			f.write(contents)
			self.hosted_sites.add(url)
			self.allocated_space -= os.path.getsize(f'{self.folder_name}/{url}/index.html')

			self.tokens += payment_requested
			out_lock.acquire()
			print(f'> [{self.id}] received {payment_requested} tokens. Balance: {self.tokens}')
			out_lock.release()


	def handle_visit_request(self, msg, sender):
		req_id = msg[0]
		url = msg[1]
		requester = Peer(msg[2][0], msg[2][1])
		payment = msg[3]

		if req_id in self.received_requests:
			return
		else:
			self.received_requests.add(req_id)

		if not url in self.hosted_sites:
			for neighbor in [n for n in self.neighbors if not n == Peer(sender[0], sender[1])]:
				msg_type = 'FIND_HOST_VISIT'
				self.send_message(msg_type, msg, neighbor)
		else:
			if not requester in self.neighbors:
				self.contact_peer(requester)
			with open(f'{self.folder_name}/{url}/index.html') as f:
				contents = f.read()
			self.send_message("ACCEPT_VISIT", [req_id, contents], requester)

	def handle_visit_accept(self, msg, sender):
		req_id = msg[0]
		contents = msg[1]
		peer = Peer(sender[0], sender[1])
		if not req_id in self.pending_requests:
			return
		req = self.pending_requests.pop(req_id)
		url = req[0]
		visit_cost = int(req[1])

		if self.verify_visit(url, contents):
			print(f'> [{self.id}] downloading website from node {peer.id}')
			try:
				shutil.rmtree(f'{self.folder_name}/visits/{url}')
			except:
				pass
			os.mkdir(f'{self.folder_name}/visits/{url}')
			f = open(f'{self.folder_name}/visits/{url}/index.html', 'w+')
			f.write(contents)

		payment_block = TransactionBlock(
			index=len(self.blockchain),
			previous_block_hash=self.blockchain[-1].hash,
			data=Transaction(sender=self.id, recipient=peer.id, amount=visit_cost)
		)
		self.tokens -= visit_cost
		block_id = uuid4()
		recipients = (list(self.neighbors) + [Peer(self.id, self.public_key)])
		random.shuffle(recipients)
		for neighbor in recipients:
			msg_type = 'UNMINED_BLOCK_PUBLISHED'
			msg = [block_id, payment_block.copy()]
			self.send_message(msg_type, msg, neighbor)

		out_lock.acquire()
		print(f'> [{self.id}] paid {visit_cost} tokens. Balance: {self.tokens}')
		out_lock.release()

		msg_type = 'VISIT_PAY'
		msg = [visit_cost]
		self.send_message(msg_type, msg, peer)

	def handle_visit_pay(self, msg, sender):
		payment = msg[0]

		self.tokens += int(payment)
		out_lock.acquire()
		print(f'> [{self.id}] received {payment} tokens. Balance: {self.tokens}')
		out_lock.release()

	def handle_update_notice(self, msg, sender):
		req_id = msg[0]
		url = msg[1]
		contents = msg[2]
		hash = msg[3]
		signature = msg[4]
		owner_id, owner_pk = msg[5]

		if req_id in self.received_requests:
			return
		else:
			self.received_requests.add(req_id)

		if not url in self.hosted_sites:
			msg_type = 'UPDATE_NOTICE'
			for neighbor in [n for n in self.neighbors if not n == Peer(sender[0], sender[1]) and not n == Peer(owner_id, owner_pk)]:
				self.send_message(msg_type, msg, neighbor)
		else:
			if self.verify_update(url, contents, hash, signature):
				f = open(f'{self.folder_name}/{url}/index.html', 'w')
				f.write(contents)

	def handle_mined_block(self, msg, sender):
		block = msg[0]
		mined_block_req_id = msg[1]
		unmined_block_req_id = msg[2]
		peer = Peer(sender[0], sender[1])

		# Ignore if mined_block_reqID is already in self.received_requests
		if mined_block_req_id in self.received_requests:
			return
		else:
			self.received_requests.add(mined_block_req_id)

		# Verify Block, Append to Blockchain, Update Directory
		self.blockchain_lock.acquire()
		if not block.prev_block_hash == self.blockchain[-1].hash:
			self.blockchain_lock.release()
			return
		self.blockchain.append(block)
		self.blockchain_lock.release()
		self.directory.update(block)

		# Check self.mining_threads; set kill flag if thread is trying to mine same block
		if unmined_block_req_id in self.mining_threads:
			self.mining_lock.acquire()
			self.mining_threads[unmined_block_req_id] = (self.mining_threads[unmined_block_req_id][0], True)
			self.mining_lock.release()

		# Forward to neighbors
		msg_type = "MINED_BLOCK_PUBLISHED"
		msg = [block, mined_block_req_id, unmined_block_req_id]
		for neighbor in [n for n in self.neighbors if not n == peer]:
			self.send_message(msg_type, msg, neighbor)

	def handle_unmined_block(self, msg, sender):
		req_id = msg[0]
		block = msg[1]
		if req_id in self.received_requests:
			return
		else:
			self.received_requests.add(req_id)
		miner_thread = threading.Thread(target=self.mine_block, args=(block, req_id))
		self.mining_threads[req_id] = (miner_thread, False)
		miner_thread.start()

		recipients = [n for n in self.neighbors if not n == Peer(sender[0], sender[1])]
		random.shuffle(recipients)
		for neighbor in recipients:
			msg_type = 'UNMINED_BLOCK_PUBLISHED'
			msg = [req_id, block.copy()]
			self.send_message(msg_type, msg, neighbor)

	# ****** Handlers for instructions from main ********
	def instructions_handler(self, instruction):
		msg_type, msg = instruction

		out_lock.acquire()
		print(f'> [Main -> {self.id}], Message: {msg_type}')
		out_lock.release()

		if msg_type == "EXIT":
			self.individual_exit()
		elif msg_type == "HARD_EXIT":
			self.hard_exit()
		elif msg_type == "PUBLISH":
			self.find_host_publish(msg)
		elif msg_type == 'UPDATE':
			self.publish_update(msg)
		elif msg_type == "VISIT":
			self.find_host_visit(msg)

	def individual_exit(self):
		print(f'> [{self.id}] Shutting down.')
		for peer in self.neighbors:
			self.send_message('NODE_EXITING', [[(n.id, n.public_key) for n in self.neighbors]], peer)

		shutil.rmtree(self.folder_name)
		self.running = False

	def hard_exit(self):
		shutil.rmtree(self.folder_name)
		self.running = False

	def find_host_publish(self, msg):
		allocated_tokens = msg[2]
		if allocated_tokens > self.tokens:
			out_lock.acquire()
			print(f'> [{self.id}] Not enough tokens to publish.')
			out_lock.release()
			return
		file_path = msg[1]
		file_size = os.path.getsize(file_path)
		req_id = uuid4()
		self.pending_requests[req_id] = msg

		for peer in self.neighbors:
			msg_type = 'FIND_HOST_PUBLISH'
			msg = [req_id, file_size, (self.id, self.public_key)]
			self.send_message(msg_type, msg, peer)

	def find_host_visit(self, msg):
		url = msg[0]
		visit_cost = int(msg[1])
		if self.tokens < int(visit_cost):
			out_lock.acquire()
			print(f'> [{self.id}] Not enough tokens to visit.')
			out_lock.release()
			return

		req_id = uuid4()
		self.pending_requests[req_id] = msg

		for peer in self.neighbors:
			msg_type = 'FIND_HOST_VISIT'
			msg = [req_id, url, (self.id, self.public_key), visit_cost]
			self.send_message(msg_type, msg, peer)

	def publish_update(self, msg):
		req_id = uuid4()
		url = msg[0]
		file_path = msg[1]

		with open(file_path, 'r') as f: contents = f.read()
		hash = str(hashlib.sha256(contents.encode()).hexdigest())
		signature = b64encode(rsa.sign(hash.encode(), import_key(self.private_key)))
		for neighbor in self.neighbors:
			msg_type = 'UPDATE_NOTICE'
			sent_msg = [req_id, url, contents, hash, signature, (self.id, self.public_key)]
			self.send_message(msg_type, sent_msg, neighbor)

		# Publish website update block
		website_block = WebsiteBlock(
			index=len(self.blockchain),
			previous_block_hash=self.blockchain[-1].hash,
			directory_hash=self.directory.hash(),
			data=DirectoryUpdate(
				type=DirectoryUpdateType.UPDATE,
				url=url,
				file_hash=hash,
				owner_pk=self.public_key.decode(),
				signature=signature
		))

		block_id = uuid4()
		recipients = (list(self.neighbors) + [Peer(self.id, self.public_key)])
		random.shuffle(recipients)
		for neighbor in recipients:
			msg_type = 'UNMINED_BLOCK_PUBLISHED'
			msg = [block_id, website_block.copy()]
			self.send_message(msg_type, msg, neighbor)

#############################################################
# 							TEST							#
#############################################################
if __name__ == '__main__':
	# ***** Network objects ******
	node_network = {}
	instruction_queues = {}
	threads = []

	# Create Seed
	seed_public, seed_private = generate_keys()
	seed_queue = Queue()
	seed_instructions = Queue()
	node_network[seed_public] = seed_queue
	instruction_queues['0'] = seed_instructions
	seed = Node(node_network, '0', seed_public, seed_private, seed_instructions)
	threads.append(seed)
	seed.start()

	# Create Nodes
	for i in range(3):
		id = str(i+1)
		public, private = generate_keys()
		node_queue = Queue()
		instructions = Queue()
		node_network[public] = node_queue
		instruction_queues[id] = instructions

		node = Node(node_network, id, public, private, instructions, ('0', seed_public), allocated_space=100000000000000)
		if id == '2': node.host = True
		node.start()
		threads.append(node)

	# Publish site
	time.sleep(8)
	print('******************')
	instruction_queues['1'].put(('PUBLISH', ['hello.com', 'WebPages/hi.html']))
	time.sleep(8)
	print('******************')
	instruction_queues['3'].put(('VISIT', ['hello.com']))
	time.sleep(8)
	print('******************')
	instruction_queues['1'].put(('UPDATE', ['hello.com', 'WebPages/hi2.html']))

	for thread in threads:
		thread.join()
