import datetime
from enum import Enum
from Block import Block

class Transaction:
    def __init__(self, sender="0", recipient="0", amount="0"):
        self.sender = str(sender).zfill(64)
        self.recipient = str(recipient).zfill(64)
        self.amount = str(amount).zfill(64)
        self.timestamp = str(datetime.datetime.utcnow().timestamp())

    def __repr__(self):
        return f'TRANSACTION\tSENDER:{self.sender}\tRECIPIENT:{self.recipient}\tAMOUNT:{self.amount}\tTIMESTAMP:{self.timestamp}'

    def __str__(self):
        return self.__repr__()

class TransactionBlock(Block):
    def __init__(self, index="".zfill(64), timestamp="", data=Transaction(), nonce="0", previous_block_hash=""):
        self.index = str(index).zfill(64)
        self.timestamp = str(timestamp)
        self.data = str(data)
        self.nonce = str(nonce)
        self.previous_block_hash = str(previous_block_hash)
        self.miner_reward = 15
        self.directory_hash="0"
        self.hash = ""

    def copy(self):
        new = TransactionBlock(
            index=self.index,
            timestamp=self.timestamp,
            data=self.data,
            nonce=self.nonce,
            previous_block_hash=self.previous_block_hash
        )
        new.hash = self.hash
        return new

    def __repr__(self):
        return f'<TRANSACTION BLOCK\tINDEX:{self.index}\tNONCE:{self.nonce}\tHASH:{self.hash}\tPREVIOUS BLOCK HASH:{self.previous_block_hash}\tTIMESTAMP:{self.timestamp}\t[{self.data}]>'

    def __str__(self):
        return self.__repr__()