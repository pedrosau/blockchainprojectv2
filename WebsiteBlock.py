import hashlib
from datetime import datetime
from enum import Enum
from Block import Block

class DirectoryUpdateType(Enum):
	PUBLISH = 0
	UPDATE = 1

class DirectoryUpdate:
	def __init__(self, type=DirectoryUpdateType.PUBLISH, url="", file_hash="", owner_pk="", signature=""):
		self.type = type
		self.url = str(url)
		self.file_hash = str(file_hash)
		self.owner_pk = str(owner_pk)
		self.signature = signature
		self.timestamp = str(datetime.utcnow().timestamp())

	def __repr__(self):
		return f'WEBSITE\tTYPE:{self.type.value}\tURL:{self.url}\tFILE_HASH:{self.file_hash}\tOWNER_PK:{self.owner_pk}\tSIGNATURE:{self.signature}\tTIMESTAMP:{self.timestamp}'

	def __str__(self):
		return self.__repr__()

class WebsiteBlock(Block):
	def __init__(self, index=''.zfill(64), timestamp=datetime.utcnow().timestamp(), data=DirectoryUpdate(), previous_block_hash='', directory_hash='', nonce='0'):
		self.index = str(index).zfill(64)
		self.timestamp = str(timestamp)
		self.data = str(data)
		self.nonce = str(nonce)
		self.previous_block_hash = str(previous_block_hash)
		self.directory_hash = str(directory_hash)
		self.miner_reward = 15
		self.hash = ''

	def copy(self):
		new =  WebsiteBlock(
			index=self.index,
			timestamp=self.timestamp,
			data=self.data,
			previous_block_hash=self.previous_block_hash,
			directory_hash=self.directory_hash,
			nonce=self.nonce
		)
		new.hash = self.hash
		return new

	def __repr__(self):
		return f'<WEBSITE BLOCK\tINDEX:{self.index}\tNONCE:{self.nonce}\tHASH:{self.hash}\tPREVIOUS BLOCK HASH:{self.previous_block_hash}\tDIRECTORY HASH:{self.directory_hash}\tTIMESTAMP:{self.timestamp}\t[{self.data}]>'

	def __str__(self):
		return self.__repr__()